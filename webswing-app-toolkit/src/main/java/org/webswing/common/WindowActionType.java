package org.webswing.common;

public enum WindowActionType {
    cursorChanged,
    close,
    minimize,
    maximize,
    move,
    resizeBottom,
    resizeRight,
    resizeUni
}
